/// @description Insert description here
// You can write your code in this editor

// Skip current frame
if(global.Pause == true || global.cameraFreeLook == true)
{
	exit;	
}

#region Get input

move		= keyboard_check(ord("D")) - keyboard_check(ord("A"));
var _jump	= keyboard_check_pressed(vk_space);

if(_jump && place_meeting(x, y + 1, oSolid))
{
	heightStart = y;
	heightEnd	= 0;

	vspd = -jumpHeight;
}

#endregion

