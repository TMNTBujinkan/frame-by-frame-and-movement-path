/// @description Insert description here
// You can write your code in this editor

collisionSystemType	= "two";	// one, two
move				= 0;
hspd				= 0;
oldHspd				= 0;
vspd				= 0;
oldVspd				= 0;
spd					= 6;
acc					= 0.4;
fric				= 0.3;
grav				= 1;
jumpHeight			= sqrt(2 * grav * 220);
maxFallSpeed		= 25;
heightStart			= 0;
heightEnd			= 0;

spriteBboxTop		= sprite_get_bbox_top(sprite_index)		- sprite_get_yoffset(sprite_index);
spriteBboxBottom	= sprite_get_bbox_bottom(sprite_index)	- sprite_get_yoffset(sprite_index);
spriteBboxLeft		= sprite_get_bbox_left(sprite_index)	- sprite_get_xoffset(sprite_index);
spriteBboxRight		= sprite_get_bbox_right(sprite_index)	- sprite_get_xoffset(sprite_index);

CameraViewRegionCreate(c_green, 1, 3);
MovementTrailCreate(4, c_yellow, 1, 1000, 2);






