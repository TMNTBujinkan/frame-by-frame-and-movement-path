/// @description Insert description here
// You can write your code in this editor

CameraViewRegionDraw();
MovementTrailDraw();

draw_set_halign(fa_center);
draw_set_valign(fa_middle);

#region Debug stuff

draw_text(x, y - 100, "hspd       : " + string_format(hspd, 1, 2));
draw_text(x, y - 120, "vspd       : " + string_format(vspd, 1, 2));

var _jumpHeight = heightStart - heightEnd;
_jumpHeight = (_jumpHeight >= 0 && vspd >= 0) ? _jumpHeight : 0;
draw_text(x, y - 150, "jumpHeight : " + string_format(_jumpHeight, 1, 2));

draw_text(x, y - 170, "view       : " + string(CameraGetViewProperties(global.xFrom, global.yFrom, global.zFrom, oCamera.fov, oCamera.aspectRatio)));

#endregion

draw_self();

