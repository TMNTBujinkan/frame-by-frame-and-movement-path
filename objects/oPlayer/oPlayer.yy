{
    "id": "1f52c8f2-d434-4478-9b0a-60b5edd2c845",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "d7cf3729-1ddf-4893-beb4-0c4ab0299845",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f52c8f2-d434-4478-9b0a-60b5edd2c845"
        },
        {
            "id": "123a0ffa-d6f1-432d-872d-7892d4273947",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "1f52c8f2-d434-4478-9b0a-60b5edd2c845"
        },
        {
            "id": "2303661a-0ace-4eb8-9be3-fbd306395840",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f52c8f2-d434-4478-9b0a-60b5edd2c845"
        },
        {
            "id": "3bd2fcff-5bbd-49fb-800b-f52beb592863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "1f52c8f2-d434-4478-9b0a-60b5edd2c845"
        },
        {
            "id": "445629fa-0eac-479a-b08b-ba40170ec531",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1f52c8f2-d434-4478-9b0a-60b5edd2c845"
        },
        {
            "id": "4f276080-4944-49b0-b7ef-edfc27e3fedf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1f52c8f2-d434-4478-9b0a-60b5edd2c845"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6eec7662-f8b5-4ee0-a8c9-1b6a3db70332",
    "visible": true
}