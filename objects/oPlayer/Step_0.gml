/// @description Insert description here
// You can write your code in this editor

// Skip current frame
if(global.Pause == true)
{
	exit;	
}

#region Update oldHspd and hspd

oldHspd = hspd;

if(move != 0)
{
	hspd = Approach(hspd, spd * move, acc * global.scale);
}
else
{
	hspd = Approach(hspd, 0, fric * global.scale);
}

#endregion

#region Collision system

	switch(collisionSystemType)
	{
		case "two":
		{
			#region Collision system type two
	
				#region Hor collision
	
				if(place_meeting(x + hspd * global.scale, y, oSolid))
				{
					while(!place_meeting(x + sign(hspd), y, oSolid))
					{
						x += sign(hspd);	
					}
	
					oldHspd	= 0;
					hspd	= 0;
				}

				x = x + (oldHspd + hspd) * 0.5 * global.scale;
	
				#endregion

				#region Ver collision
	
				// Gravity
				oldVspd = vspd;
				vspd	= Approach(vspd, maxFallSpeed, grav * global.scale);

				if(place_meeting(x, y + vspd * global.scale, oSolid))
				{
					while(!place_meeting(x, y + sign(vspd), oSolid))
					{
						y += sign(vspd);	
					}
	
					oldVspd	= 0;
					vspd	= 0;
				}

				y = y + (oldVspd + vspd) * 0.5 * global.scale;
	
				#endregion

			#endregion
		
			break;
		}
		
		default:
		{
			#region Collision system type one
	
				#region Hor collision

				x = x + (oldHspd + hspd) * 0.5 * global.scale;

				if(place_meeting(x, y, oSolid))
				{
					var _solidId = instance_place(x, y, oSolid);
	
					if(hspd > 0)
					{
						x = (_solidId.bbox_left - 1) - spriteBboxRight;	
					}
					else if(hspd < 0)
					{
						x = (_solidId.bbox_right + 1) - spriteBboxLeft;
					}
	
					oldHspd = 0
					hspd	= 0;
				}

				#endregion

				#region Ver collision

				// Gravity
				oldVspd = vspd;
				vspd	= Approach(vspd, maxFallSpeed, grav * global.scale);

				if(place_meeting(x, y + 1, oSolid) && vspd >= 0)
				{
					oldVspd = 0;
					vspd	= 0;
				}

				y = y + (oldVspd + vspd) * 0.5 * global.scale;

				if(place_meeting(x, y, oSolid))
				{
					var _solidId = instance_place(x, y, oSolid);
	
					if(vspd > 0)
					{
						y = (_solidId.bbox_top - 1) - spriteBboxBottom;	
					}
					else if(vspd < 0)
					{
						y = (_solidId.bbox_bottom + 1) - spriteBboxTop;
					}
	
					oldVspd = 0;
					vspd	= 0;	
				}
	
				#endregion
	
			#endregion
		}
	}
	
#endregion

// Calculate jump height
if(vspd >= 0 && heightEnd == 0 && !place_meeting(x, y + 1, oSolid))
{
	heightEnd = y;
}