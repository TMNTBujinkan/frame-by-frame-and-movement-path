/// @description Insert description here
// You can write your code in this editor

/*
// Anti-aliasing
display_reset(2, false);

// Zbuffer
gpu_set_zwriteenable(true);
gpu_set_ztestenable(true);

*/

currentView				= 0;
fov						= 70;
width					= 1600;
height					= 900;
aspectRatio				= (width / height);
cameraMoveSpeed			= 8;
cameraSensitivity		= 0.5; // [0.1 to 10]
cameraEasing			= 8;
global.cameraFreeLook	= false;
global.cameraReset		= true;
global.freeCameraX		= 0;
global.freeCameraY		= 0;
global.freeCameraZ		= 0;
global.xFrom			= width  * 0.5;
global.yFrom			= height * 0.5;
global.zFrom			= CameraSetProperZFrom();
global.xTo				= global.xFrom;
global.yTo				= global.yFrom;
global.zTo				= 1000;
global.camPitch			= 0;
global.camDir			= -90;

forwardKey	= 0;
backwardKey	= 0;
rightKey	= 0;
leftKey		= 0;
upKey		= 0;
downKey		= 0;

// Set game window position and size
var _posX	= (display_get_width()  * 0.5) - (width * 0.5);
var _posY	= (display_get_height() * 0.5) - (height * 0.5);

window_set_size(width, height);
window_set_position(_posX, _posY);

// Resize application surface and gui
surface_resize(application_surface, width, height);
display_set_gui_size(width, height);

// Create camera and set it's update script
global.camera = camera_create();
camera_set_view_pos(global.camera, 0, 0);
camera_set_view_size(global.camera, width, height);

camera_set_update_script(global.camera, CameraUpdate);

if(room == rmInit)
{
	room_goto_next();	
}


