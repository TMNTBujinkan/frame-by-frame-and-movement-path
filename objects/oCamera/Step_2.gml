/// @description Insert description here
// You can write your code in this editor

if(instance_exists(oPlayer))
{
	// Get player previous and current position
	with(oPlayer)
	{
		var _playerX			= x;	
		var _playerY			= y;
	}
	
	// Reset camera to its default values
	if(global.cameraReset)
	{
		global.cameraFreeLook	= false;
		global.cameraReset		= false;
		global.freeCameraX		= 0;
		global.freeCameraY		= 0;
		global.freeCameraZ		= 0;
		global.xFrom			= _playerX;
		global.yFrom			= _playerY;
		global.zFrom			= CameraSetProperZFrom();
		global.xTo				= global.xFrom;
		global.yTo				= global.yFrom;
		global.zTo				= 1000;
		global.camPitch			= 0;
		global.camDir			= -90;
	}
	
	// Follow player when free look is off
	if(global.cameraFreeLook == false)
	{		
		global.xFrom += (_playerX - global.xFrom) / cameraEasing;
		global.yFrom += (_playerY - global.yFrom) / cameraEasing;
	}
}