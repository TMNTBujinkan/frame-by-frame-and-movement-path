/// @description Insert description here
// You can write your code in this editor

// Enable view port
view_enabled = true;
view_set_visible(currentView, true);

// Set view port variables
view_set_xport(currentView, 0);
view_set_xport(currentView, 0);
view_set_wport(currentView, width);
view_set_hport(currentView, height);

// Set projection matrix and link camera to view port
projectionMatrix = matrix_build_projection_perspective_fov(fov, aspectRatio, 1.0, 3200.0); 
camera_set_proj_mat(global.camera, projectionMatrix);

view_set_camera(currentView, global.camera);

// Set mouse cursor
var _displayWidthHalf  = display_get_width()  * 0.5;
var _displayHeightHalf = display_get_height() * 0.5;

display_mouse_set(_displayWidthHalf, _displayHeightHalf);

