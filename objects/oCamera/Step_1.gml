/// @description Insert description here
// You can write your code in this editor

global.cameraFreeLook	^= keyboard_check_pressed(vk_f1);
global.cameraReset		^= keyboard_check_pressed(ord("R"));

if(global.cameraFreeLook)
{
	var _cameraSen = (min(max(0.1, cameraSensitivity), 10)) * 0.1;
	
	#region Get input

	forwardKey	= keyboard_check(ord("W"));
	backwardKey	= keyboard_check(ord("S"));
	rightKey	= keyboard_check(ord("D"));
	leftKey		= keyboard_check(ord("A"));
	upKey		= keyboard_check(vk_space);
	downKey		= keyboard_check(ord("C"));

	#endregion

	#region Free look camera mouse movement

	window_set_cursor(cr_none);

	var _displayWidthHalf  = display_get_width()  * 0.5;
	var _displayHeightHalf = display_get_height() * 0.5;

	global.camDir	-= (display_mouse_get_x() - _displayWidthHalf) * _cameraSen;

	global.camPitch = clamp(global.camPitch - (display_mouse_get_y() - _displayHeightHalf) * _cameraSen, -89, 89);

	display_mouse_set(_displayWidthHalf, _displayHeightHalf);

	#endregion

	#region Free look camera keyboard movement

	if(forwardKey)
	{
		global.freeCameraX -= dcos(global.camDir)		* cameraMoveSpeed;
		global.freeCameraY -= dsin(global.camPitch)		* cameraMoveSpeed;
		global.freeCameraZ -= dsin(global.camDir)		* cameraMoveSpeed;
	}
	
	if(backwardKey)
	{
		global.freeCameraX += dcos(global.camDir)		* cameraMoveSpeed;
		global.freeCameraY += dsin(global.camPitch)		* cameraMoveSpeed;
		global.freeCameraZ += dsin(global.camDir)		* cameraMoveSpeed;
	}

	if(rightKey)
	{
		global.freeCameraX -= dsin(global.camDir)	* cameraMoveSpeed;
		global.freeCameraZ += dcos(global.camDir)	* cameraMoveSpeed;
	}

	if(leftKey)
	{
		global.freeCameraX += dsin(global.camDir)	* cameraMoveSpeed;
		global.freeCameraZ -= dcos(global.camDir)	* cameraMoveSpeed;
	}
	
	if(upKey)
	{
		global.freeCameraY -= dsin(90) * cameraMoveSpeed;
	}
	
	if(downKey)
	{
		global.freeCameraY -= dsin(270) * cameraMoveSpeed;
	}

	#endregion
}
else
{
	window_set_cursor(cr_default);	
}

