{
    "id": "998628e9-1a13-4d5d-843e-b8cc963b22da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCtrl",
    "eventList": [
        {
            "id": "a7af6989-94c0-460f-a189-731f9d6e9473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "998628e9-1a13-4d5d-843e-b8cc963b22da"
        },
        {
            "id": "73206eb8-ee3d-48c6-8e63-c94865dd9e69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "998628e9-1a13-4d5d-843e-b8cc963b22da"
        },
        {
            "id": "d10c2e65-ac5f-4c96-bf87-834229fb7f59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "998628e9-1a13-4d5d-843e-b8cc963b22da"
        },
        {
            "id": "e7356bd8-6d28-41a9-9a1c-7f262c632b55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "998628e9-1a13-4d5d-843e-b8cc963b22da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}