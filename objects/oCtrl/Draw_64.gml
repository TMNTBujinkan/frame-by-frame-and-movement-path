/// @description Insert description here
// You can write your code in this editor

draw_set_halign(fa_left);
draw_set_valign(fa_middle);

draw_text(20, 20,  "Scale : " + string(global.scale));
draw_text(20, 50,  "[ Press Shift to toggle slow motion ]");
draw_text(20, 80,  "[ Press Enter to toggle pause ]");
draw_text(20, 110, "[ Press F in pause mode to test game frame by frame ]");
draw_text(20, 140, "[ Press F1 to toggle free look camera ]");
draw_text(20, 170, "[ Press R to reset camera ]");
draw_text(20, 200, "[ Press Tab to reset game ]");
draw_text(20, 230, "[ Press ESC to end game ]");

draw_set_color(c_yellow);
draw_text(20, 300, "[ You can move camera in free look mode with WASD, Space and C ]");
draw_set_color(c_white);