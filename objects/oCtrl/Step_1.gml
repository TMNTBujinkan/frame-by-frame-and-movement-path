/// @description Insert description here
// You can write your code in this editor

var _pauseKey			= keyboard_check_pressed(vk_enter);
var _frameByFrameKey	= keyboard_check_pressed(ord("F"));

// Pause game
if(_pauseKey)
{
	global.Pause = !global.Pause;	
}

// Frame by frame test
if(global.Pause == true && _frameByFrameKey == true)
{
	global.Pause		= false;
	global.FrameByFrame = true;
}

// Set game speed scale
if(keyboard_check_pressed(vk_shift))
{
	global.scale = (global.scale == 1) ? global.scaleTraget : 1;	
}

// Exit game
if(keyboard_check_pressed(vk_escape))
{
	game_end();
}

// Reset game
if(keyboard_check_pressed(vk_tab))
{
	game_restart();	
}