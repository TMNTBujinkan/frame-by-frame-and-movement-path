/// @func	CameraGetViewProperties()
/// @desc	returns viewX, viewY, viewWidth and viewHeight of our perspective camera.

/// @param cameraXFrom
/// @param cameraYFrom
/// @param cameraZFrom

var _cameraXFrom		= argument[0];
var _cameraYFrom		= argument[1];
var _cameraZFrom		= abs(argument[2]);

with(oCamera)
{
	var _cameraFov			= fov;
	var _cameraAspectRatio	= aspectRatio;
}

var _viewHeight = 2 * dtan(_cameraFov * 0.5) * _cameraZFrom;
var _viewWidth	= _viewHeight * _cameraAspectRatio;

var _viewX	= _cameraXFrom - (_viewWidth * 0.5);
var _viewY	= _cameraYFrom - (_viewHeight * 0.5);

return[_viewX, _viewY, _viewWidth, _viewHeight];

