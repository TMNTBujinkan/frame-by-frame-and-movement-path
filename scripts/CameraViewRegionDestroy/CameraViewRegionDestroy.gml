/// @func	CameraViewRegionDestroy()

vertex_format_delete(cameraViewVertexFormat);
vertex_delete_buffer(cameraViewVertexBuffer);