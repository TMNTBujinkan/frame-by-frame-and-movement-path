/// @func	CameraViewRegionUpdate()

var _result = CameraGetViewProperties(global.xFrom, global.yFrom, global.zFrom);

vertex_begin(cameraViewVertexBuffer, cameraViewVertexFormat);

/*

Helper

_result[0]	==>	viewX
_result[1]	==> viewY
_result[2]	==>	viewWidth
_result[3]	==>	viewHeight

*/

vertex_position_3d(cameraViewVertexBuffer, _result[0], _result[1], viewRegionZ);
vertex_color(cameraViewVertexBuffer, viewRegionColor, viewRegionAlpha);

vertex_position_3d(cameraViewVertexBuffer, _result[0] + _result[2], _result[1], viewRegionZ);
vertex_color(cameraViewVertexBuffer, viewRegionColor, viewRegionAlpha);

vertex_position_3d(cameraViewVertexBuffer, _result[0] + _result[2], _result[1] + _result[3], viewRegionZ);
vertex_color(cameraViewVertexBuffer, viewRegionColor, viewRegionAlpha);

vertex_position_3d(cameraViewVertexBuffer, _result[0], _result[1] + _result[3], viewRegionZ);
vertex_color(cameraViewVertexBuffer, viewRegionColor, viewRegionAlpha);

vertex_position_3d(cameraViewVertexBuffer, _result[0], _result[1], viewRegionZ);
vertex_color(cameraViewVertexBuffer, viewRegionColor, viewRegionAlpha);

vertex_end(cameraViewVertexBuffer);







