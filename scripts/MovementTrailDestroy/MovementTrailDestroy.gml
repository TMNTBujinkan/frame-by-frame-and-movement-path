/// @func MovementTrailDestroy()

ds_list_destroy(trailList);
vertex_format_delete(movementTrailVertexFormat);
vertex_delete_buffer(movementTrailVertexBuffer);