/// @func	CameraViewRegionDraw()
/// @desc	draws camera view region.

vertex_submit(cameraViewVertexBuffer, pr_linestrip, -1);