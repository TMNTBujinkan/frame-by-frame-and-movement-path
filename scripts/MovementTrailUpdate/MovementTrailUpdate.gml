/// @func MovementTrailUpdate()

#region Fill the trail list and keep its size constant

var _disToPreviousPosition = point_distance(xCurrent, yCurrent, x, y);

if(_disToPreviousPosition >= trailMinDistanceToAddPoint)
{
	xCurrent	= x;
	yCurrent	= y;
	trailLength += _disToPreviousPosition;
	
	ds_list_add(trailList, [xCurrent, yCurrent, _disToPreviousPosition]);	
}

var _array;
while(trailLength > trailMaxLength)
{
	_array = trailList[| 0];
	trailLength -= _array[2];
	
	ds_list_delete(trailList, 0);
}

#endregion

#region Fill the vertex buffer

vertex_begin(movementTrailVertexBuffer, movementTrailVertexFormat);

var _currentPos, _nextPos, _direction, _lengthDirX, _lengthDirY;
_trailListSize = ds_list_size(trailList);

for(var _i = 0; _i < _trailListSize; _i += 1)
{
	/*
	Helper
	
	_currentPos[0]	==> x
	_currentPos[1]	==> y
	_currentPos[2]	==> distance to previous point
	
	_nextPos[0]		==> x
	_nextPos[1]		==> y
	_nextPos[2]		==> distance to previous point
	
	*/
	
	_currentPos = trailList[| _i];
	
	if(_i == _trailListSize - 1)
	{
		_nextPos = _currentPos;
	}
	else
	{
		_nextPos = trailList[| _i + 1];
	}
	
	_direction	= point_direction(_currentPos[0], _currentPos[1], _nextPos[0], _nextPos[1]) + 90;
	_lengthDirX	= lengthdir_x(trailWidthHalf, _direction);
	_lengthDirY	= lengthdir_y(trailWidthHalf, _direction);
	
	vertex_position(movementTrailVertexBuffer, _currentPos[0] + _lengthDirX, _currentPos[1] + _lengthDirY);
	vertex_color(movementTrailVertexBuffer, trailColor, trailAlpha);
	
	vertex_position(movementTrailVertexBuffer, _currentPos[0] - _lengthDirX, _currentPos[1] - _lengthDirY);
	vertex_color(movementTrailVertexBuffer, trailColor, trailAlpha);
}

vertex_end(movementTrailVertexBuffer);

canDrawMovementTrail = true;

#endregion
