/// @func Approach()

/// @param start
/// @param end
/// @param shift

var _start	= argument[0];
var _end	= argument[1];
var _shift	= argument[2];

if(_start < _end)
{
	_start = min(_start + _shift, _end);	
}
else
{
	_start = max(_start - _shift, _end);	
}

return(_start);