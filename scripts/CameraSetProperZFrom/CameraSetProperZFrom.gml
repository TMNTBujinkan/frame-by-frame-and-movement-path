/// @func	CameraSetProperZFrom()
/// @desc	returns proper zFrom based on camera fov, width and height of camera's view.

with(oCamera)
{
	var _cameraViewHeight	= height;
	var _cameraFov			= fov;
}

var _zFrom = -(_cameraViewHeight / (2 * dtan(_cameraFov * 0.5)));

return(_zFrom);
