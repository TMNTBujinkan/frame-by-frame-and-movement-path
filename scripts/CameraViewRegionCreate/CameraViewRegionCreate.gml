/// @func	CameraViewRegionCreate()

/// @param	viewRegionColor
/// @param	viewRegionAlpha
/// @param	viewRegionZ

viewRegionColor = argument[0];
viewRegionAlpha = argument[1];
viewRegionZ		= argument[2];

vertex_format_begin();
vertex_format_add_position_3d();
vertex_format_add_colour();
cameraViewVertexFormat = vertex_format_end();

cameraViewVertexBuffer = vertex_create_buffer();