/// CameraUpdate()

var _xUp	= 0;
var _yUp	= 1;
var _zUp	= 0;

global.xTo	= (global.xFrom + global.freeCameraX) - dcos(global.camDir) * dcos(global.camPitch);
global.yTo	= (global.yFrom + global.freeCameraY) - dsin(global.camPitch);
global.zTo	= (global.zFrom + global.freeCameraZ) - dsin(global.camDir) * dcos(global.camPitch);

lookAtMatrix = matrix_build_lookat(	global.xFrom + global.freeCameraX, 
									global.yFrom + global.freeCameraY, 
									global.zFrom + global.freeCameraZ, 
									global.xTo, global.yTo, global.zTo, 
									_xUp, _yUp, _zUp);
									
camera_set_view_mat(view_camera[0], lookAtMatrix);