{
    "id": "159e1c48-28cb-4d07-8614-19b5ac6b031b",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnOne",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "248903c8-61a1-4ef1-9a9a-50e04a334922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a28a835f-8ddd-4c26-9ab4-94dbbfb85521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9d19dd4b-989d-4f81-ae3b-1dcf237a7e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "35c549bf-bfec-4c88-b8d4-61aabdc3f3b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "827bfac6-a7af-404e-a4d9-06b96c6b2d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "caf02ace-a112-4ee1-a269-139019cf067f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 18,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a4e38acd-9f3a-47b6-ba56-d043e71325a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3ba4b4cc-889c-4d48-a423-a444ca2218ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6c72386b-c73a-40a4-b620-8adba59b639f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 237,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ed3bc7fc-4852-4666-902a-d958cad289d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e2e86927-78fe-4df1-83e4-8ce3aaf3b2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c6d6abfa-8514-49b4-abbd-04232c0f34bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 216,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b957c264-3a45-4423-91b2-dd9999ae06d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 198,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a9dc4360-9438-4002-b2c2-f79d6883bf8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0f7717ec-3f25-4ef1-ad3f-d987ffdf41b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0b620146-474c-47c1-a4d2-f1c57e8f870c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "40947ac8-b82a-4b24-90bb-fad2f87a42fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "475b4ee1-1ce2-48ff-a1ee-cbf5114cd7ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d10ec66d-312a-4566-a28d-b1ed01742855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "877223b6-8e8f-4afa-ac86-444033a56f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 128,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0f58aa68-62b4-40e6-97dd-2f8a7a2d11e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9dfbae09-911a-44a5-b6d6-e83b833e293b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "03cbb316-0281-428a-944e-49052c8fdcec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 89,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "55f08c06-710c-4180-8557-37907a24ba8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8ba91a69-0d7e-433c-acec-8b5e22dc823c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "99320f84-1165-4c44-861d-6df69e4fb45f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 150,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9728e464-b356-4b79-9953-bd9616dd246f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 145,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "780d1544-bc60-4357-9d51-67f39e05ff60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5ffc9faf-4e0a-4d49-ac10-5b2e8ff3dcd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ce2fb43f-15ab-478e-8e6d-6443f30d5016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bbf83739-287a-405b-b2d9-6c79d742f6ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 101,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bc98ad80-d2b3-4ea8-9e4d-eef30398df17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 88,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "89fb9d99-de9e-442e-9a57-1cc11e8caf6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5633fe59-f45a-4444-acc6-41d566a7104a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 48,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8855e841-e722-40ab-8d60-352dadbc945f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8438d813-a1ee-4df2-89a9-29ab3da52810",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b1d67112-b60f-45f7-998b-c1910b0752ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c3a0ce3e-fcbf-45f3-a0cb-69112203f143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3a7b442a-af8a-43d8-9d57-f23d73293ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "51f875c1-8163-4407-8c6d-d85c2e222d51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 202,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c90d4b35-5b27-4261-81ed-771d7033469a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e4eb6999-8e2a-4db9-835d-f9de21826de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a7caca63-d94c-46ab-87ec-c33665ad5a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6d594338-5cc8-4ee1-a1a9-42ec8b47459e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c169d311-8854-448f-8dde-0b8fc8d127b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 144,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0cf63a3d-4a19-4d1f-8bc5-6c4d38fdc684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ced7dd1e-abdd-45ef-ba9a-96bb686c1152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "70ae6384-4499-4ad8-9938-ee94f9e1e70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 83,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a0bc2561-bf41-4813-a409-b2bf24312d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0dd2ed6a-675b-4222-aacc-0b902a53871f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "11127fac-d615-4b68-973d-e2fdbdecd976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "57798f7d-74db-4ab7-b388-ff5f85dd8688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "856eb7f9-cb1d-4804-8936-108e266fc429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ce12c967-3ed5-4d69-8d6b-4a436b5fbf04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8ab5d225-d8db-4ead-99a0-21e212081bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6b5623bb-494b-4e20-a5cb-fec68ad9c166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7f86f41a-ee62-4172-9f56-e7b20d2eefd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9d49acb4-04c0-4b89-81d7-1efd9c745270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4e3d94ea-3c8f-4d4c-98dc-dd06e0728945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b18d6bc6-0943-4482-bc14-f3d27de5a070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 33,
                "y": 29
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e6027a7c-3620-45bf-abb4-d1a70cae2557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "166d200d-7703-4cdb-81b0-11a8d87ff0fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4525e4e3-2e61-43c6-b8b0-2a3575c24658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1b9a97c8-0dd8-4810-810a-7ae2f7bfd3f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "659d426f-2fac-4a15-81d3-d4cbcbf209ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dd5f5994-d101-4d2d-96cf-c4e17c98ef89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ed9420b4-35fa-4e09-8909-0018dfce57e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9b0fb068-b8d7-47fd-b9a5-9d78b62e3319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f9641d68-646d-4fc2-bb5a-16d261f55bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "de154e58-69d1-42d4-9ce7-fcb4ce153aeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ffd5d586-be90-4363-a520-a7a583a98acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "040e3ab0-d9a2-4fb2-86c1-964394e3130a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "133dad08-4d9f-4e99-913e-775491babea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 180,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e3e8130c-e47e-402a-8d0b-26c32acb8aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 71,
                "y": 29
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0bfeabaa-47ca-493f-9fe8-f61eb8e026f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "baa1e8fc-f319-4597-b3c2-e5b8d7bf64a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5dca44a6-bd90-4bf0-b404-44fd8e410899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 32,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "88b4c850-e9e1-47dc-ae73-037d526ce5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "74985f11-f73d-448b-9ed5-892595aa6661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e3a3e198-f7df-4dcf-8620-e9962ad6c0bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fb96e726-8ff1-4884-9934-f4d430bb9da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "323d205a-8bf3-437e-a964-ed4b84d70132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 209,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e72ce633-28fc-4b5f-9e75-6c8a899b1a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e68b5308-d7ba-4d16-839d-d3aa78ee3c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "47b2fb5a-b975-4a6d-b4ce-078debae7440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "90fde5dc-1440-489d-9de0-9c1f2f739ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "818bd12d-12a1-49f0-b4a1-d40226356d84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 155,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "da212371-2491-47d7-91dd-963c716beed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "05f58f54-7222-451d-97be-506323b9321d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "61aa451d-a546-49f8-8728-0aefd504b221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fdc33994-3a32-41d8-8c08-36da410070a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d6fc7648-2a9b-4f08-97a1-cf3b3353753b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "23ab35d2-946f-4365-996b-8182f8e483cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7ab77a8c-b142-4614-a484-a48daa5e2350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 29
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a50b517c-a628-41bb-8dec-8f8ed20af58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 163,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3d7d77d1-4413-43c7-b365-e5d9cb068cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 177,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ac0acbed-da0d-4231-a8e4-43f825234142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "f1a744f7-4e48-431b-9c66-97bf7bc19cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "20a85678-1006-4eb9-a46f-dfc4af4b6cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "a17058c2-7191-446a-a82f-fb095b719e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "8da7de55-57f6-4793-a579-c51ce2f29b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "01f1cf18-4015-4f43-bb2f-f836a46e5d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "e2582116-9510-4201-a26c-f8a30f43fd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "9fee6f77-c49d-4755-99a0-e58b8f608498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "18954abf-9c4d-4948-a9b4-a397a5ca5dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "f12131f4-91be-432f-8a7d-330782d88db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "7d6593c5-8e3a-4997-9343-d9f6dec55484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "756cb091-03b5-4912-b753-9f70e720d65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "1ac8f3eb-0062-4b5b-ae48-8054d31558ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "47820897-6c35-4caf-9693-c9049f8e6eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "961ce87c-54e6-4948-8eab-bf9fdb39645c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "51bb3aba-9a2b-44a4-a4a1-003a9f43497a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "c6608da6-be49-4dbb-9d89-a490c88f890f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "03475add-2453-4214-8261-963638565ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "6765a49e-9626-41d5-9835-226a72003240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "fdb048bb-fb3f-455f-a004-411f67b11823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "366ca006-8841-4b02-96b5-1157e3109ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "ce482d8e-554f-4b42-81c8-3ae37e8a9ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "84c5060c-f414-4a37-940b-8056a469f8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "84eb1b3f-cd34-432d-a4d3-9e1b27cbe1b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "51a5b711-e702-4f04-abea-7997dc1a67e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "2c4e7c4b-714c-4688-8766-160c84a51110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "0a27f1f6-26a5-4d73-9b4f-86b2ebbdfe12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "9fa4fcd8-4d5d-4ac6-8181-288a8281e289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "00004649-f33e-4d8d-ad25-55ef0001313f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "d58e2fa1-8a6c-43cb-b076-90943c46aea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "06b4c863-d9dc-4610-84cf-65d21684b24b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "555fbdcd-b9ed-4add-9162-8d9443e72c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "f2420448-26c2-44a5-aeba-bb638b787e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "88e61831-444b-461b-88d0-13bf2707656a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "5f56e98c-468a-4c47-a656-6d5bd0a13925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "13430198-7c29-47ea-a44a-80581e0284ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "3c83385e-15de-49d4-bd8a-cd39eb74ddc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "b9cd6c04-2c6c-4348-affe-86faf1c8329a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "f517ed4a-c2aa-445a-acd1-9767d970d98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "1397250e-9bc4-49e6-b63b-0ba58cac459c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "816260a5-c059-4f8f-93e7-9799fd6faf30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "73afa89b-f0c9-4d0b-a706-ac81f27cea6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "82f62ffc-05f4-4da4-b5f9-ceac7378c8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "785a58e8-a0db-4020-a7a4-5359d94b687b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "cb0792c0-b1a3-48b2-8e4f-585f3e71abd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "a108ad63-54ac-49f6-92f0-e40af4439a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "6327c3a3-f6a2-44dd-919c-9ca855e762bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "813bb482-f66a-4167-99d8-e62f34741f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "105d12d0-e68e-4e85-85e6-30650507d299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "2df19045-6cf2-4d6b-b1df-265457c1d6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "ffabe6fe-da03-4c36-854b-c36579d8c634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "fc0698b3-4dd4-43bb-8075-b3a80388de63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "6def4563-9e80-4f85-8772-1a41f477fae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "eaf27e64-7a48-403a-bd30-713b9c6a3a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "dc8c49db-6fc5-4758-b568-16351ccbdb45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "87628f4f-12ea-49c3-aaa9-11e1235b4d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "c1e49667-0209-4fc7-bdd0-dfca5b7a068a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "84415f86-577c-45f8-937a-4c9acb158659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "1b82c2e2-d884-486e-a8e2-223cfa7beda3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "d48797d7-c62a-435a-96b2-fe39568ea4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "28e199ba-196b-42dc-8605-9d96d52ae9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "8ec8af86-d6da-447e-8924-9dce6478f14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "c6ee3eb3-28a8-471c-a0de-9c669603f981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "28d0e946-2808-44eb-9d1a-dc7a095e69b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "2334ea81-9e35-4210-8cd6-6387ec58fb91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "f90dc513-42a6-451a-b596-038fbee62269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "9cbad47c-74a8-4ffd-808c-9efaab6bb2c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "7f31c394-96a2-4113-a5fc-2f562daa603a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "893730f6-366b-4a40-b1ae-85f24bc7c2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "2127779e-85bd-4800-891a-671dfe69ea04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "29b8f1ae-b1ca-4c25-8068-431ccb07cab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "161d4514-7da7-49e4-80e7-b5c168e48a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "df399fc8-7cbf-469c-9aa9-f451f6d1ce6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "b34c187b-5570-4ffd-9032-914278fd5818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "cdc1535e-25ce-4385-8602-d9485d8fbd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "525ec272-3ba3-4f80-940b-41ae84a46d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "cec748b7-2eea-4d53-9d82-d3519b7bbe0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "450d99d8-0909-4f5b-8a0c-9e6085643473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "63381e4a-f200-498a-8488-3bc3768e7c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "9955fcdb-0bbb-4c75-a279-33d8f12756d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "faeca628-e64e-4660-a2ca-9b2ea474bff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "7f626002-2e3e-4234-9435-fc41835a2da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "c6456c61-d452-4b78-90ff-c6be4c6e532e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "a5122435-7a7b-433d-acb9-08a3b7b9e2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "4c3120fd-7f23-4f2f-bd04-a00384d507b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "e72a4ccf-84a5-4b08-ad43-597e85197151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "2d971e6b-d1f5-438e-8ff1-6cb9b43a8aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "9e3b9081-6ad4-4c97-83fc-9683e2d52c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}