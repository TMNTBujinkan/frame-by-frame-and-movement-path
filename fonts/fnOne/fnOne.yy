{
    "id": "159e1c48-28cb-4d07-8614-19b5ac6b031b",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnOne",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bd7c460e-3e2f-4d6a-811e-230af7697573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "fd356dae-aff8-4731-a3fb-844d147cd64c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 10,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f1df5909-13a1-4f7e-9430-ac15e2c14868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c6a10675-27f9-43ad-8091-af7451c5f9af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 242,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5e9b524f-0059-4142-b37a-7e8380fd42d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f63fb2d6-dec5-49f4-bdde-2ee422811c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 213,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9287b2f9-7e51-4b86-99f0-8d51d41dc6f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 198,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4ce07782-2ff6-4a11-afcd-65e7af676420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 193,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e4eb3684-d297-47e5-964f-679df0ff3b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 186,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "dc28e522-86e2-49c4-ae32-52a873abd3b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 179,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "fd829564-c094-44c9-8a11-27b46b8d97f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 15,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "93ec96af-99e7-4535-b07e-786e0a7c4eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9c45b4d2-3515-4f1a-8b32-1888393ef107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 150,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aa3e94c2-ad13-4571-8768-1db870171888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9deae17a-4965-4a25-8b93-c5b9abdaa1e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 137,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b3914cc2-d570-4836-816a-f5d7a084f90a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "13f63540-f735-4d7d-9ded-78480655cce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 117,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3aa81891-705a-44d2-9817-3a884179f1b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 109,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "48bd957b-d231-40cd-b875-87f018d8f41e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 97,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1a274275-4c2b-46fe-9b66-5cd596fc893a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 85,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d36892e0-6f0c-40aa-8810-2d4502181d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 73,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6e06dc49-9a66-450e-bf00-60ceced58630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 155,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8c1352ad-58ec-49ea-9bb6-2cb901c90b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 24,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1550310d-0d60-4265-b033-d5cc8ef40d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5c248f70-84b4-4fe3-b088-e2d5a5159ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9f6ff06a-56c8-4030-b00b-13513ccbf10d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 60,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8d83fb2e-03bb-4d06-981c-9505ac1c58b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 55,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2699ebe3-4938-40b5-b75f-24ad4f5af3ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 50,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "313f27ee-a948-4116-9fa3-f680906c9a1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "873ce815-f5cc-4012-8b74-975245e571dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4a1553cb-7d23-4866-8eb5-8b480ea1c4e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "77e4770f-814c-4bcd-b0c8-385fc34862e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e896aeaa-d80e-47c6-ae3c-b000e3c86a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 229,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f0dd7a06-15f7-4b1c-93ce-876c80f6b731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ee3a6226-70d9-40ec-9f5f-fff4470cee36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 200,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ab9ef557-f62e-4013-8411-f31dc0016afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 185,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "adc58848-dc0a-459f-a794-39bc36b9c391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 171,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "65111a83-9185-4a27-adaa-65f2bbc98a58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9f931d4f-f98e-4800-ad70-3750ba34b5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2858de85-90b7-4c4d-82d7-df47e01fb870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 131,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e78c5a07-c2de-4029-acc0-4297d30e06cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 117,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "132f62e1-93d5-42ef-bc3b-91851aa0c0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f44dcb5b-51c0-44c4-8914-4b8f558def54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "95301ff5-acb5-409c-90b7-b53f758824ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 87,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "42041636-ebd7-4265-9c06-a151d9aa7ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 76,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "11a9a221-dff5-4448-bcad-095613817661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 60,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "184a9231-84eb-4f76-8689-87b6a409628f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e35eca43-c902-40fb-a92d-0fc14050e191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ab47479c-c5ed-4679-83f8-2c7828f5fab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 30,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a99cfc8f-89ea-46fe-ae2f-2abd0d8ed90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 23,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "064baaa4-18c4-4339-a370-ee41eb9a2f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8f5b7058-1046-4f52-8fdb-a386e52e02be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d684bff9-c5c5-493b-b687-60687600c5f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b42d82be-3de9-44b8-a157-78462a54d397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8b0e2b14-f8c5-451f-a966-b6e3e144ad12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "32f91c35-99fa-4e02-8e1b-e442c4695468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d4e8540a-e890-49e4-9cb7-9ce6e25aedb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e9877f43-016d-4ff3-93c6-d168a063d1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2c59b232-3be5-427a-b399-89cb52618347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "51eeefe4-b639-4192-9480-16378db0b0f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 17,
                "y": 26
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5b710ba2-4777-49c3-93c2-f9aa4c3af265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ec87febf-247e-418c-9eb0-4b017429ce7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "44d70766-be37-48e4-a0ed-ba87ea52f614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "939d1128-de31-4072-9869-7ff664857469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ce022f34-9c82-45ca-9608-c8b8dcd367a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5d71cd88-67d9-461e-bbdc-4575d1eec42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f55e8ea0-317f-40f5-919c-d2dafd851da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7f171709-2746-467c-8deb-d63fd351776b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1f8fef07-72d1-405c-8831-a92da835059b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5eccd2ce-e017-444a-921f-6936a94c7d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d4eaa255-8993-4b93-922d-68032d156c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "31ebf520-a792-45d4-868c-a0f62c0cfa9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 40,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0d0af4cb-ea9e-477f-9207-88fe1ad8618e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "47f7b2c2-c781-4acf-82a6-3d0d4cb9509d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 52,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fec86643-4cd7-4a5a-8266-f20d997d46d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "46c93783-4438-4dad-b2e6-8dcfcbdb1064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3d04a656-4532-4b6b-8e54-f62bf8e362d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 241,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5a799dc2-01ad-41ea-8ca0-d176d7746e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 225,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7404aa70-32e1-4e05-b41d-a57591699526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 214,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d7e5dff4-3f38-4fc5-b996-a33d3e0a8ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 202,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "15f23c32-fbd9-4884-908d-5f037f9aca99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 191,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2e79b80d-9538-474b-9268-915fcce17058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a135507a-c4e2-4a32-9d3a-0680b3817757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 171,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3918a4e2-275c-40b2-b327-b8fe5159b18c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 19,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4cb652aa-06e3-4c4b-90bb-0426f977eba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 163,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9f8a632f-7110-44b0-891b-d7c550e39e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "702bfb80-1df2-4afb-9435-909d56d343e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "44fcfbfe-18e1-4865-a054-d9130614608f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 113,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f2d97133-85e7-4f88-aaec-ef217df4c58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 101,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0ebfbdaa-88e8-4e33-a212-8fd57ad0d844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 89,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2376e46b-333b-4166-9667-92a8226b4fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 77,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "de2ba939-a36f-405a-b4cb-fb675bfa9142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bcc40268-e3ed-4286-b6c6-66c1cf0c4c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 64,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "50e03071-7033-4332-ac53-dd4a61b5339d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0f5c1a15-9b5b-4833-ad55-89319d6bc1a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 72,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "77c5e1e6-5f47-4501-b0ed-8c50df6cf1e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 85,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "0293df4f-eb92-4986-90d5-68ddcbf5d603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "d8e225fd-cfc9-4faf-b558-8caeffda4bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "3b6e8019-c048-40c6-bfbf-77cc2eaf0a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "d5b68e5a-eb6d-4101-893c-0e0a645a9fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "19fc7143-7d63-44a9-9941-fc0fb5199e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "5c4b5bba-75b0-4a19-bf36-0d6c0fa1c4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "75218bb0-ecb1-4588-b792-3e7f631d8cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "c5d81af6-fa1b-4d90-8f5a-866a236d5b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "71a3d93f-2d9a-42c7-a263-a0d78b541f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "0c4ee195-b721-4c31-afc8-ad418d086973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "90a4dfa8-69a9-44c5-8a59-acb365dc6798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "9daf9887-0a6b-4b8a-9c03-877ad5cf3429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "6fdcf607-ce4e-40b3-8ae9-d3eec3bf99f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "0cdfbf1b-c849-4046-b74d-7ee05cd9af38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "73d53aba-b123-4c22-9aa5-f4ef9b62cdb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "964b0060-0a8e-4cf8-9d39-7574db23472b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e453d773-8a84-446b-97a1-d6d0f6a0aa5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "11785e39-e792-4e4c-8ee9-08fc5fc33557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "e4eff789-0dea-4e0a-bebe-32db38327353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "8208b748-96a4-4426-aa87-ec1db6786e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "82690805-908d-4e2c-befc-76f582c6181b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "2c82e4bb-b3ca-46b0-8760-361551143197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "f221ce28-9a12-4c54-83d7-83e891717bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "992e356b-2c52-4e83-a8a8-56f53b4bbd9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "52b42b8d-4ef8-493e-8ae5-277ee2b966e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "982b992b-6adf-4ef4-a56c-10ef4a00fbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "271a67c2-40b2-448a-9d8c-23129b1787df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "9835f998-8efe-4200-b733-7d619114d1d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "64b99648-be12-46ce-a8f9-3009b7b945d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "655e2eed-8cb5-406c-8217-8fa9cc0b1c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "5e2bbd2b-c129-4aaa-803e-dee66d831b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "9914e41f-c2be-483e-a356-26c6f389eae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "2acacb85-709d-4f14-9e9e-add654319cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "36636fe0-3fb0-4abc-a6e2-fb20e44b4344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "c0632355-19d9-4b1b-9192-20c721a4e739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "85bac367-a69b-4e0d-ab0e-5f9b06253aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "ed9a6411-9155-4fc9-bfeb-51e52a0aae9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "d8121943-5902-4dcf-98cc-14cc2b468863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "b35e8ff7-9db4-4d71-9d2e-dba61856c24b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "ffc917ce-709a-4bf3-a318-c3827ad71a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "8ac6a162-1fcc-49ac-983c-87374166c5fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "d3c947aa-1db8-4113-b54a-59638d62f037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "d4332374-b19d-4b46-9d06-e34b3712f2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "d258311c-0b1c-42aa-87a6-36517f12f222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "98f16be1-a918-4930-ad1c-bd53d6684209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "c1d61fbb-10b9-4471-bd82-9cced3e150be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "e13e6486-e928-4636-aac8-ff3ce49d2691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "b0ffbb4b-7c41-4503-8bf0-c58408f1ea04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "23f5b803-7691-4de8-ac06-f77dd2faf3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "c4ec51da-847a-4f84-87cf-24a3484fb882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "eceb317f-81a5-4811-bdb4-0586982aca50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "417fefcb-a49b-4210-9b2a-05d5ec603357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "99224ad8-02b4-4c82-8037-f153eaa90084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "dd49d815-9856-4155-976b-99b4046b3b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "b7cbbc22-1f3a-419a-9697-dd45db2e87c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "bed5fc9d-b90f-4604-b4dd-6d3953dfdba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "24986456-0b6d-4356-b615-2e52afd2bcf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "b6ab3f8b-e805-4bf7-862a-c8d843855e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "6e006a62-7c69-4ced-81ac-5adf4b7b4659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "2addfa63-36fd-47d3-b96b-f1e8d5304462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "7b7e7d0b-624d-4f38-8db0-75d044ad83d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "c7aac983-4b9c-4246-a57c-9aae8ec1770a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "3ad9e907-2c9d-41c7-b041-fd65a3f1b452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "b9e0982a-72f7-45ad-9dd3-f8fa58b075bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "0f3b9473-027b-4b36-b552-9162a20d49d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "87fd69bb-6186-4d1b-9f63-cced224477dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "680d903b-e019-4c0b-b11d-c25b24314c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "98c701a6-b1db-4964-a41f-abca9b156cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "148cbaf8-963e-4feb-bc2b-f476b0edc418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "c1a09313-7e5e-49f7-9979-236b0dbb3939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "88a177b2-8963-414a-bb2c-b75e2bd27a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "5f3636c1-4ee4-44e1-9fca-de25f4dafe11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "d259c92c-af3c-4d3b-869f-ba1bc2e2c013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "3612fba7-383a-4f06-a7ad-3fe534c0f587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "3f63ee70-1166-479c-a6ce-b7eea459b2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "225a757e-9698-406c-b60d-63227c2e4db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "94479855-c7e3-4fe5-b2eb-125e8bc5c933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "44554c30-451b-4d66-9769-f397698e40aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "8efb546c-20c8-4964-84b3-ee6acb631c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "7fba4405-8426-4ef0-b1c3-f3f4e8d5d699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "a5efe2ab-3dde-4e98-a1f3-9f6d3147b875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "ea35d862-99bc-4841-a143-1b7c4df74df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "67ed30eb-b651-4495-b602-444c415f4739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "490d98c7-b37c-43f5-a87a-d13d59e53b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "0e2fb40f-19ed-48e8-ab35-58e7a1e1ef29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "be552e24-1ae6-422f-87bd-352c2ebc898b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "03149eba-dc74-4d54-8a39-6054b7a0b095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "d90971ff-664f-4fa5-8728-1fd8e877d503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}